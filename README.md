# Detección de intrusos en una red usando Machine Learning Supervisado en comparación con Teoría Bayesiana 

En esta guía, se pretende guiar a ejecutar el código proporcionado en Python en los sistemas operativos Windows y Linux. Cubriremos los siguientes pasos:

1. **Descargar e Instalar Python:**
   - Windows: Descarga el instalador de Python desde [python.org](https://www.python.org/downloads/windows/). Ejecuta el instalador y sigue las instrucciones.
   - Linux: Python generalmente ya está instalado en la mayoría de las distribuciones de Linux. Puedes verificar su presencia ejecutando `python --version` o `python3 --version` en la terminal. Si no está instalado, utiliza el gestor de paquetes de tu distribución para instalarlo.

2. **Instalar Bibliotecas Requeridas:**
   - Abre una terminal en Windows (puedes usar el comando `cmd`) o en Linux.
   - Para instalar las bibliotecas necesarias, ejecuta el siguiente comando:
   
     ```
     pip install pandas scikit-learn
     ```

3. **Crear un Entorno Virtual (Opcional pero Recomendado):**
   - Para mantener el entorno de trabajo limpio, puedes crear un entorno virtual. Aquí te mostramos cómo hacerlo:
   
     - Windows:
       ```
       pip install virtualenv
       virtualenv myenv
       .\myenv\Scripts\activate
       ```

     - Linux:
       ```
       pip install virtualenv
       virtualenv myenv
       source myenv/bin/activate
       ```

4. **Descargar y Preparar el Conjunto de Datos:**
   - Asegúrate de que el archivo "KDDTrain+.txt" esté en la ubicación especificada en el código. La cual es 
    ```
     nsl-kdd/KDDTrain+.txt
     ```

5. **Ejecutar el Código:**
   - En la misma terminal donde activaste tu entorno virtual (si lo creaste), navega al directorio que contiene el código Python.
   - Ejecuta el código Python:

     ```
     python tu_script.py
     ```

   Sustituye "tu_script.py" por el nombre del archivo que contiene el código Python.

6. **Resultado:**
   - El código entrenará un modelo de RandomForest o un modelo Bayesiano, según sea el caso y mostrará la precisión del modelo y un informe de clasificación en el conjunto de prueba.

Con estos pasos, deberías poder ejecutar el código Python en Windows o Linux. Asegúrate de tener las bibliotecas necesarias instaladas y de que el conjunto de datos esté en la ubicación correcta.
